from random import randint
from time import time

print("EP 1")


# print("Limite: %s" % (time() - limite_tempo))
def criar_vetor_aleatorio(l, h, tam):
    vetor = []

    for i in range(tam):
        vetor.append(randint(l, h))

    return vetor


def selecao(v):
    resp = []

    while v:
        m = min(v)
        resp.append(m)
        v.remove(m)

    return resp


def insercao(v):
    for j in range(1, len(v)):
        x = v[j]
        i = j - 1
        while i >= 0 and v[i] > x:
            v[i + 1] = v[i]
            i = i - 1
        v[i + 1] = x
    return v


ordenacao = [insercao, selecao]
ordenados = []
tempos = []
t_vetor = 1000

for k in range(1, 3):
    for x in ordenacao:
        aleatorio = criar_vetor_aleatorio(1, 10000, t_vetor)
        print(aleatorio)
        start = time()
        ordenado = x(aleatorio)
        tempo_exec = time() - start
        ordenados.append(ordenado)
        tempos.append(tempo_exec)
        t_vetor += t_vetor

